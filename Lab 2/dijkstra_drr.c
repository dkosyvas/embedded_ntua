#include <stdio.h>
#include "drr.h"

#define NUM_NODES                          100
#define NONE                               9999

#if defined(SLL)
cdsl_sll *nodeList;
#elif defined(DLL)
cdsl_dll *nodeList;
#else
cdsl_dyn_array *nodeList;
#endif
             
int AdjMatrix[NUM_NODES][NUM_NODES];

int g_qCount = 0;
NODE rgnNodes[NUM_NODES];
int ch;
int iPrev, iNode;
int i, iCost, iDist;


void print_path (NODE *rgnNodes, int chNode) {
  if (rgnNodes[chNode].iPrev != NONE) {
      print_path(rgnNodes, rgnNodes[chNode].iPrev);
  }
  printf (" %d", chNode);
  fflush(stdout);
}


void enqueue (int iNode, int iDist, int iPrev) {

  QITEM *qNew = (QITEM *) malloc(sizeof(QITEM));
  
  if (!qNew) {
      fprintf(stderr, "Out of memory.\n");
      exit(1);
  }

  qNew->iNode = iNode;
  qNew->iDist = iDist;
  qNew->iPrev = iPrev;
  qNew->qNext = NULL;

  /* Add the qNext element to the end of the nodeList struct */
  nodeList->enqueue(0, nodeList, (void*)qNew);
  g_qCount++;

}


void dequeue (int *piNode, int *piDist, int *piPrev) {

#if defined (SLL)
  iterator_cdsl_sll it;
#elif defined (DLL)
  iterator_cdsl_dll it;
#else
  iterator_cdsl_dyn_array it;
#endif

  /* Pointer qKill points to the first element of the struct (which will be deleted) */
  it = nodeList->iter_begin(nodeList);
  QITEM *qKill = (QITEM*)(nodeList->iter_deref(nodeList, it));

  if (qKill) {
    /* Save the data of the first struct element so as to return them to main function */
    *piNode = qKill->iNode;
    *piDist = qKill->iDist;
    *piPrev = qKill->iPrev;

    /* Delete the first element of the struct */
    nodeList->dequeue(0, nodeList);
    g_qCount--;
  }
}


int qcount (void)
{
  return(g_qCount);
}

int dijkstra(int chStart, int chEnd) {

  for (ch = 0; ch < NUM_NODES; ch++) {
    rgnNodes[ch].iDist = NONE;
    rgnNodes[ch].iPrev = NONE;
  }

  if (chStart == chEnd) {
    printf("Shortest path is 0 in cost. Just stay where you are.\n");
  } else {

    rgnNodes[chStart].iDist = 0;
    rgnNodes[chStart].iPrev = NONE;
     
    enqueue (chStart, 0, NONE);
      
    while (qcount() > 0) {
      dequeue (&iNode, &iDist, &iPrev);
      for (i = 0; i < NUM_NODES; i++) {
        if ((iCost = AdjMatrix[iNode][i]) != NONE) {
          if ((NONE == rgnNodes[i].iDist) || (rgnNodes[i].iDist > (iCost + iDist))) {
            rgnNodes[i].iDist = iDist + iCost;
            rgnNodes[i].iPrev = iNode;
	    enqueue (i, iDist + iCost, iNode);
	  }
	}
      }
    }
      
    printf("Shortest path is %d in cost. ", rgnNodes[chEnd].iDist);
    printf("Path is: ");
    print_path(rgnNodes, chEnd);
    printf("\n");
  }
}

int main(int argc, char *argv[]) {
  int i,j,k;
  FILE *fp;
  
  if (argc<2) {
    fprintf(stderr, "Usage: dijkstra <filename>\n");
    fprintf(stderr, "Only supports matrix size is #define'd.\n");
  }

  /* Open the adjacency matrix file */
  fp = fopen (argv[1],"r");

  /* Make a fully connected matrix */
  for (i=0;i<NUM_NODES;i++) {
    for (j=0;j<NUM_NODES;j++) {

      /* Make it more sparce */
      fscanf(fp,"%d",&k);
      AdjMatrix[i][j]= k;

    }
  }

  /* Initialize the data struct */
#if defined (SLL)
  nodeList = cdsl_sll_init();
#elif defined (DLL)
  nodeList = cdsl_dll_init();
#else 
  nodeList = cdsl_dyn_array_init();	
#endif

  /* Find 10 shortest paths between nodes */
  for (i=0,j=NUM_NODES/2;i<20;i++,j++) {
    j=j%NUM_NODES;
    dijkstra(i,j);
  }
  exit(0);
}