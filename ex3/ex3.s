.text

    .align		4

    .global		my_strlen
    .type		my_strlen, %function
    .global		my_strcpy
    .type		my_strcpy, %function
    .global		my_strcat
    .type		my_strcat, %function
    .global		my_strcmp
    .type		my_strcmp, %function

/* r0:	input string*/

my_strlen:

    push {r1,r2,r7,lr}  	/* lr is link register used to hold the return address for a function call.  */ 
    mov	r1, r0
    mov	r0, #0       		/*number of characters in string   */

    /* Iterate until '\0' */
    loop_strlen:
        ldrb r2,[r1], #1    	/* load byte from memloc[r1++] into r2 (postincrement r1) */
        cmp r2, #0
        beq end_strlen
        add r0,#1
        b loop_strlen

    end_strlen:

    pop	{r1,r2,r7,pc}

/*
 *   r0:	destination
 *   r1:	source
 *   returns: 	destination
 */

my_strcpy:

    push {r1,r2,r3,r7, lr}
    mov	r2, r0

    /* Iterate and copy over until '\0' */
    loop_strcpy:
        ldrb r3, [r1], #1 	/*  loads byte from memloc[r1++] into  r3 (postincrement r3) */
        strb r3, [r2], #1 	/* write contents of  r3 register to r2 and updated r2 address  */

        cmp r3, #0
        bne loop_strcpy

    pop	{r1,r2,r3,r7, pc}


/*
 *   r0:	destination
 *   r1:	source
 *   returns: 	destination
 */
my_strcat:

    push {r1,r2,r3,r7, lr}
    mov	r2, r0

    /* Reach the \0 of dest */
    first_loop:
        ldrb r3, [r2], #1
        cmp r3, #0
        bne first_loop
    
    /* Move one back to point to \0 */

    sub	r2, r2, #1  		/* due to postincrement of r2 in ldrb */
    second_loop:
        ldrb r3, [r1], #1 	/* r3 = r1[j] */
        strb r3, [r2], #1 	/* r0[i+j] = r3 */

        cmp r3, #0
        bne second_loop

    pop	{r1,r2,r3,r7,pc}


/* r0: first string
 * r1: second string
 * strcmp returns 0 when the strings are equal, 
 * a negative integer when s1 is less than s2, 
 * or a positive integer if s1 is greater than s2
 */

my_strcmp:

    push {r1,r2,r3,r4,r7, lr}
    mov	r2, r0

    strcmp_l:
        ldrb r3, [r2], #1	/*  r3 = &s1 */
        ldrb r4, [r1], #1	/*  r3 = &s2 */

        subs r0, r3, r4 	/* enable positive/negative or zero  return  with subs */ 

        bne strcmp_end

        /* Chars are equal, check if we reached the end */
        cmp r3, #0
        bne strcmp_l

    strcmp_end:

    pop	{r1,r2,r3,r4,r7, pc}

