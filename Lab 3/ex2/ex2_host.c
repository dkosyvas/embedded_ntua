#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>

int main()
{
	int fd, count;
	char buf[64];
	char data[2];

	printf("Give a string of up to 64 bytes:\n");
	count = read(0, buf, 64);
	if (count < 0) printf("Read failed\n");

	/* The virtual port changes every time QEMU launches */
	fd = open("/dev/pts/5", O_RDWR);
	if (fd < 0) printf("open failed\n");
	if (write(fd, buf, count) < 0) printf("Write failed\n");

	/* Ignore the input string which is printed in the port */
	do {
		count = read(fd, data, 1);
		if (count < 0) printf("read from qemu failed\n");
	} while (data[0] != '\n');

	/* Read the most frequent charcter and its frequency */
	read(fd, &data[0], 1);
	read(fd, &data[1], 1);

	printf("Most frequent character is %c and appeared %d times\n", data[1], data[0]);

	close(fd);
	return 0;
}
