.text
.global main

main:

	mov r0, #0		/* Zero memory allocated to arr */
	mov r1, #0 
	ldr r2, =arr
zero:	strb r0, [r2, #1]!
	add r1, r1, #1
	cmp r1, #93
	ble zero

        ldr r0, =port      	/* open /dev/ttyAMA0 */
	mov r1, #0x2		/* O_RDWR */
	mov r7, #5
	swi 0

	mov r12, r0		/* r0 and r12 now contain the file descriptor */

        ldr r1, =buf    	/* buffer where input string is stored */
        mov r2, #64     	/* number of bytes to read */
        mov r7, #3
        swi 0

	ldr r3, =buf		/* r3 -> &buf */
	ldr r10, =arr		/* r10 -> &arr */
	sub r3, r3, #1
	mov r7, #0

loop:	ldrb r4, [r3, #1]!	/* r4 -> buf[i] */ 
	add r6, r10, r4		/* r6 -> &arr + buf[i] */
	sub r6, r6, #33		/* Ignore characters with ascii code <= 32 */
	ldrb r5, [r6, #0]	/* r5 -> arr[buf[i]] */
	add r5, r5, #1		/* arr[buf[i]]++ */
	strb r5, [r6, #0]

	add r7, r7, #1
	cmp r7, r0
	blt loop

	ldr r1, =arr		/* r1 -> &arr */
	sub r1, r1, #1
	mov r2, #0		/* r2 holds arr max */
	mov r3, #93
	mov r4, #0		/* r4 -> arr iterator */
	mov r6, #0		/* r6 holds the index of the max */
	
max:	ldrb r5, [r1, #1]!	/* r5 -> arr[i] */
	cmp r5, r2		/* if arr[i] > max */
	addgt r2, r5, #0	/* max = arr[i] */
	addgt r6, r4, #33	/* max_index = i */
	add r4, r4, #1
	cmp r3, r4
	bgt max

	/* Store max and index in freq array */

	ldr r0, =freq		
	strb r2, [r0, #0]!
	strb r6, [r0, #1]!
	mov r6, #10		/* new line for better visualization */
	strb r6, [r0, #1]!
	
	mov r0, r12		/* write the result to /dev/ttyAMA0 */
	ldr r1, =freq
	mov r2, #3
	mov r7, #4
	swi 0
	
	mov r7, #6		/* close(fd) */
	swi 0

	mov r0, #0		/* exit(0) */
	mov r7, #1
	swi 0

.data
	port: .ascii "/dev/ttyAMA0"
        buf: .ascii "\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0 "
        /* pre-allocate 64 bytes for input string, initialize them with null character '/0'*/
	arr: .skip 93
	freq: .skip 3
	fd: .skip 8

