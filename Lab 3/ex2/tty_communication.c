#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>

int main()
{
//	FILE *pf_cat, *pf_echo;
	int fd, count;
	char buf[64];
	char data[64];
	char *cat_command = "cat /dev/pts/0";
	char *kill_cat_command = "killall cat";
//	char *echo_command = "echo Hello world! | tee /dev/pts/0";

	printf("Give a string of up to 64 bytes:\n");
	count = read(0, buf, 64);
	if (count < 0) printf("Read failed\n");

	fd = open("/dev/pts/0", O_RDWR);
	if (fd < 0) printf("open failed\n");
	if (write(fd, buf, count) < 0) printf("Write failed\n");

/*	pf_echo = popen(echo_command, "r");
	if (pf_echo < 0) printf("cat failed'n");
	pf_cat = popen(cat_command, "r");
	if (pf_cat < 0) printf("cat failed'n");
	fgets(data, 64, pf_cat);
*/
	system(cat_command);
	system(kill_cat_command);
	count = read(fd,data,64);
	if (count < 0)
		printf("read from qemu failed\n"); 
	else
		printf("Received from qemu: %s", data);

//	pclose(pf_echo);
//	pclose(pf_cat);
//	close(fd);
	return 0;
}
