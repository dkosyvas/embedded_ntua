.text
.global main
.extern printf

main:

start:  mov r0, #1              /* stdout */
        ldr r1, =output_string  /* memory where output string is stored */
        mov r2, #len            /* number of bytes of output message */

        mov r7, #4      /* number of write system call */
        swi 0

        mov r0, #0      /* stdin */
        ldr r1, =buf    /* buffer where input string is stored */
        mov r2, #32     /* number of bytes to read */
        mov r7, #3
        swi 0

        /* Check for ending sequence */
        mov r5, r0
        cmp r5, #2
        beq check

        /* Divide input bytes to categories */
sort:   mov r6, #0
        ldr r3, =buf
        sub r3, r3, #1

loop:   ldrb r4, [r3, #1]!
        cmp r4, #48
        blt store
        cmp r4, #58
        blt number
        cmp r4, #65
        blt store
        cmp r4, #91
        blt capital
        cmp r4, #97
        blt store
        cmp r4, #122
        blt small

number: cmp r4, #53
        addlt r4, r4, #5
        subge r4, r4, #5
        b store

capital:add r4, r4, #32
        b store

small:  sub r4, r4, #32

store:  strb r4, [r3]
        add r6, r6, #1
        cmp r6, r5
        ble loop
        bgt print

print:  mov r0, #1
        ldr r1, =buf
        mov r2, r5
        mov r7, #4
        swi 0

        b start

check:  ldr r8, =buf
        ldrb r9, [r8, #0]
        cmp r9, #81
        beq exit
        cmp r9, #113
        beq exit
        bne sort

exit:   mov r0, #0
        mov r7, #1
        swi 0

.data
        output_string: .ascii "Input a string of up to 32 char long: " /* location of output string in memory */
        len = . - output_string
        buf: .ascii "\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0"
        /* pre-allocate 32 bytes for input string, initialize them with null character '/0'*/

