.text
.global main
.extern printf

main:

        mov r0, #0
        mov r1, #0
        ldr r2, =arr
zero:   str r0, [r2, #1]!
        add r1, r1, #1
        cmp r1, #93
        ble zero

start:  mov r0, #1              /* stdout */
        ldr r1, =output_string  /* memory where output string is stored */
        mov r2, #len            /* number of bytes of output message */

        mov r7, #4      /* number of write system call */
        swi 0

        mov r0, #0      /* stdin */
        ldr r1, =buf    /* buffer where input string is stored */
        mov r2, #64     /* number of bytes to read */
        mov r7, #3
        swi 0

        ldr r3, =buf            /* r3 -> &buf */
        ldr r10, =arr           /* r10 -> &arr */
        sub r3, r3, #1
        mov r7, #0

loop:   ldrb r4, [r3, #1]!      /* r4 -> buf[i] */
        add r6, r10, r4         /* r6 -> arr[0] + buf[i] */
        sub r6, r6, #33         /* Ignore characters with ascii code <= 32 */
        ldrb r5, [r6, #0]       /* r5 -> arr[buf[i]] */
        add r5, r5, #1          /* arr[buf[i]]++ */
        strb r5, [r6, #1]

        add r7, r7, #1
        cmp r7, r0
        blt loop

        mov r0, #1
        ldr r1, =arr
        mov r2, #64
        mov r7, #4
        swi 0

/*      ldr r0, =message
        push {ip, lr}
        bl printf
        pop {ip, pc}
*/

        mov r0, #0
        mov r7, #1
        swi 0

.data
        output_string: .ascii "Input a string of up to 32 char long: " /* location of output string in memory */
        len = . - output_string
        message: .ascii "Here\n"
        buf: .ascii "\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0"
        /* pre-allocate 32 bytes for input string, initialize them with null character '/0'*/
        arr: .skip 93