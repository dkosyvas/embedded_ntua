#!/bin/bash

make

for conf in sll dll dyn
do
	log_file="mem_accesses_${conf}"
	exec="dijkstra_${conf}"
	chmod +x $exec	
	valgrind --log-file=$log_file --tool=lackey --trace-mem=yes ./$exec input.dat
	echo "Memory accesses for ${conf}" >> mem_access.out
	cat $log_file | grep 'I\|L' | wc -l >> mem_access.out
	valgrind --tool=massif ./$exec input.dat
	rm $log_file
done

massif="$(echo massif.out.*)"
i=1
for massif_file in massif
do
	ms_print $massif_file > mem_footprint_$i.txt
	i=$((i+1))
done
	
make clean
