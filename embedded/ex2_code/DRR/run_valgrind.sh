#!/bin/bash

make

for cl in sll dll dyn
do
	for pk in sll dll dyn
	do
		log_file="mem_accesses_${cl}_${pk}"
		exec="drr_${cl}_${pk}"
		chmod +x $exec	
		valgrind --log-file=$log_file --tool=lackey --trace-mem=yes ./$exec
		echo "Memory accesses for CL = ${cl} and PK = ${pk}" >> mem_access.out
		cat $log_file | grep 'I\|L' | wc -l >> mem_access.out
		valgrind --tool=massif ./$exec
		rm $log_file
	done
done

make clean
	
