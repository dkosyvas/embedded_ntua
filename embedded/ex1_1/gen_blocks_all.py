import sys
from subprocess import check_output

def run_meas(ex):
    total = 0.0
    n = 500
    time_arr = []
    for i in range(n):
        t = int(check_output(ex).decode())
        time_arr.append(t)
    return (time_arr)


ex = ['./x5', '2', '176']
time_arr = run_meas(ex)
print (time_arr)
