import sys
from subprocess import check_output

def run_meas(ex):
    total = 0.0
    n = 500
    time_arr = []
    for i in range(n):
        t = int(check_output(ex).decode())
        time_arr.append(t)
    return (time_arr)

# Block Sizes common divisors of M=176, N=144
blkz = [1, 2, 4, 8, 16]

ex = ['./x4', '']
for k in blkz:
    ex[1] = str(k)
    time_arr = run_meas(ex)
    print ('Block size = ', k)
    print (time_arr)
