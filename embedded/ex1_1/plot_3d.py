import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from math import sqrt


fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')

x = []
y = []
z= []

with open('output.txt') as f:
    lines = f.readlines()
    x = [float(line.split()[0]) for line in lines]
    y = [float(line.split()[1]) for line in lines]
    z=  [float(line.split()[2]) for line in lines] 
    
ax.scatter(x,y,z)

ax.set_xlabel('Bx')
ax.set_ylabel('By')
ax.set_zlabel('Execution Time')

plt.show()
