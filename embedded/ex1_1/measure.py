import sys
import numpy as np
from subprocess import check_output



def fun(cmd,n):
    s = 0
    arr=np.zeros((n,), dtype=int)

    for i in range(n):
         arr[i]= int(check_output(cmd).decode())
         s += arr[i]

    return (float(s) / n),min(arr),max(arr)


if __name__ == '__main__':
    cmd = [
        './' + sys.argv[1]]
    times = int(sys.argv[2])
    avg,min_time,max_time=fun(cmd,times)
    print("Average time is :",avg)
    print("Minimum time is :",min_time)
    print("Max time is :",max_time)
