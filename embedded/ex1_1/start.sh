#!/bin/bash



chmod +x start.sh
chmod +x measure.py
chmod +x explore.py
chmod +x gen_blocks.py 
chmod +x plot_3d.py 


make

echo 'Original phods without transformations'  
python3 measure.py ./x1 10 
echo 'phods with merge transformation'  
python3 measure.py ./x2 10 
echo 'phods with reuse transformation'
python3 measure.py ./x3 10 
echo 'phods with reuse and macros definition transformation'
python3 measure.py ./mq 10 
  
echo 'Best block size'
python3 explore.py ./x4

rm output.txt
python3 gen_blocks.py ./x5 


#python3 plot_3d.py

rm x1 x2 x3 x4 x5 mq
