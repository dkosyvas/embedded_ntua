import sys
from subprocess import check_output

def run_avg(ex):
    total = 0.0
    n = 10
    for i in range(n):
        t = int(check_output(ex).decode())
        total += t
    return (float(total) / n)

# Block Sizes common divisors of M=176, N=144
blkz = [1, 2, 4, 8, 16]



Tm = 99999999
blk = 0

ex = ['./x4', '']
for k in blkz:
    ex[1] = str(k)
    time = run_avg(ex)
    print (k, time, sep="\t|\t")
    if (time < Tm):
        Tm = time
        blk= k

sys.stdout.write("\033[1;31m")
print (blk, Tm,sep="\t|\t")
sys.stdout.write("\033[0;0m")
