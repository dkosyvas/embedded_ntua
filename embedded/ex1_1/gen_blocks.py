#!/usr/bin/env python3
import sys
import numpy as np
from subprocess import check_output

def run_avg(cmd):
    total= 0.0
    n = 10
    for i in range(n):
        time= int(check_output(cmd).decode())
        total += time
    return (float(total)/n)


blks_x = [1,2,3,4,6,8,9,12,16,18,24,36,48,72,144]
blks_y = [1,2,4,8,11,16,22,44,88,176]


T = 123456789
blkx1 = 0
blky1 = 0
ex = ['./x5', '', '']

f = open("output.txt", "a")


for blkx in blks_x:
	ex[1] = str(blkx)
	for blky in blks_y:
		ex[2] = str(blky)
		t = run_avg(ex)
		print (blkx, blky, t, sep="\t\t",file=f)

		if (t < T):
			T=t
			blkx1 = blkx
			blky1 = blky

f.close()
print ("X_BLKS","Y_BLKS", "TIME", sep="\t\t")
sys.stdout.write("\033[0;32m")
print (blkx1, blky1, T,sep="\t\t")

sys.stdout.write("\033[0;0m")

