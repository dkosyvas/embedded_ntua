#!/bin/bash
make
cp  rand_str_input_first  m1
cp  rand_str_input_sec  m2
echo "Running for input_first"
./string_manipulation.out  rand_str_input_first 
./string_manipulation_def.out  m1
cmp --silent rand_str_input_first_concat_out m1_concat_out && echo "SUCCESS: Files Are Identical! " || echo "files are different"  #check if files are the same from build-in functions and from assembly
cmp --silent rand_str_input_first_len_out m1_len_out && echo "SUCCESS: Files Are Identical! " || echo "files are different" 
cmp --silent rand_str_input_first_sorted_out m1_sorted_out && echo "SUCCESS: Files Are Identical! " || echo "files are different" 

echo "Running for input_sec"
./string_manipulation.out  rand_str_input_sec
./string_manipulation_def.out  m2

cmp --silent rand_str_input_sec_concat_out m2_concat_out && echo "SUCCESS: Files Are Identical! " || echo "files are different" 
cmp --silent rand_str_input_sec_len_out m2_len_out && echo "SUCCESS: Files Are Identical! " || echo "files are different" 
cmp --silent rand_str_input_sec_sorted_out m2_sorted_out && echo "SUCCESS: Files Are Identical! " || echo "files are different" 



#rm     m1 m2 *_out*

