.text
.global main
.extern printf
main:
	push {ip, lr}
	ldr r0, =string
	mov r1, #15
	bl printf
	pop {ip,pc}

.data
string: .asciz
  "Number=%d\n"
