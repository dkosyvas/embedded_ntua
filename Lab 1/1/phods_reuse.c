/*Parallel Hierarchical One-Dimensional Search for motion estimation*/
/*Initial algorithm - Used for simulation and profiling             */
#include <sys/time.h>
#include <stdio.h>
#include <stdlib.h>

#define N 144     /*Frame dimension for QCIF format*/
#define M 176     /*Frame dimension for QCIF format*/
#define B 16      /*Block size*/
#define p 7       /*Search space. Restricted in a [-p,p] region around the
                    original location of the block.*/

void read_sequence(int current[N][M], int previous[N][M])
{ 
	FILE *picture0, *picture1;
	int i, j;

	if((picture0=fopen("akiyo0.y","rb")) == NULL)
	{
		printf("Previous frame doesn't exist.\n");
		exit(-1);
	}

	if((picture1=fopen("akiyo1.y","rb")) == NULL) 
	{
		printf("Current frame doesn't exist.\n");
		exit(-1);
	}

  /*Input for the previous frame*/
  for(i=0; i<N; i++)
  {
    for(j=0; j<M; j++)
    {
      previous[i][j] = fgetc(picture0);
    }
  }

	/*Input for the current frame*/
	for(i=0; i<N; i++)
  {
		for(j=0; j<M; j++)
    {
			current[i][j] = fgetc(picture1);
    }
  }

	fclose(picture0);
	fclose(picture1);
}


void phods_motion_estimation(int current[N][M], int previous[N][M],
    int vectors_x[N/B][M/B],int vectors_y[N/B][M/B])
{
  int x, y, i, j, k, l, p1, p2, q2, distx, disty, S, min1, min2, bestx, besty;
  int v1,v2;

  distx = 0;
  disty = 0;

  /*Initialize the vector motion matrices*/
  for(i=0; i<N/B; i++)
  {
    for(j=0; j<M/B; j++)
    {
      vectors_x[i][j] = 0;
      vectors_y[i][j] = 0;
    }
  }

  /*For all blocks in the current frame*/
  for(x=0; x<N/B; x++)
  {
    for(y=0; y<M/B; y++)
    {
      S = 4;

      while(S > 0)
      {
        min1 = 255*B*B;
        min2 = 255*B*B;

        /*For all candidate blocks in X dimension*/
        for(i=-S; i<S+1; i+=S)     
        {
            distx = 0;                                 
    	    disty = 0;                                 
    for (k = 0; k < B; k++) {                 
        for (l = 0; l < B; l++) {             
            v1 = B * x + k;             
            v2 = B * y + l; 
	     
            p1 = current[v1][v2];          

            v1 += vectors_x[x][y];           
            v2 += vectors_x[x][y];           
            if ((v1 + i) < 0 ||(v1 + i) > (N - 1) ||(v2) < 0 ||(v2) > (M - 1)) {            
                p2 = 0;                        
            } else {                           
                p2 = previous[v1 + i][v2]; 
            }                                  
            distx += abs(p1 - p2);             
            if ((v1) < 0 ||(v1) > (N - 1) ||(v2 + i) < 0 ||(v2 + i) > (M - 1)) {        
                q2 = 0;                        
            } else {                           
                q2 = previous[v1][v2 + i]; 
            }                                  
            disty += abs(p1 - q2);             
        }                                      
    }                                          
    if (distx < min1) {                        
        min1 = distx;                          
        bestx = i;                             
    }                                          
    if (disty < min2) {                       
        min2 = disty;                          
        besty = i;                             
    }
   }

        S = S/2;
        vectors_x[x][y] += bestx;
        vectors_y[x][y] += besty;
      }
    }
  }
} 

int main()
{  
  	int current[N][M], previous[N][M], motion_vectors_x[N/B][M/B],motion_vectors_y[N/B][M/B], i, j;
	
	struct timeval t0,t1;
	
	read_sequence(current,previous); 
 	gettimeofday(&t0, NULL);
	phods_motion_estimation(current,previous,motion_vectors_x,motion_vectors_y);
	gettimeofday(&t1, NULL);
	unsigned long long dt =t1.tv_sec*1000000 +  t1.tv_usec - t0.tv_sec*1000000 - t0.tv_usec;	


	printf ("%llu\n", dt);

  return 0;
}