#!/bin/bash





make

echo 'Tables before orio optimization'  
python3 measure.py ./x1 10 
echo 'Tables after orio optimization with Exhaustive algorithm '  
python3 measure.py ./x2 10 
echo 'Tables after orio optimization with RandomSearch (10 runs) algorithm '  
python3 measure.py ./x3 10 
echo 'Tables after orio optimization with Simplex algorithm '  
python3 measure.py ./x4 10 

rm x1 x2 x3 x4


