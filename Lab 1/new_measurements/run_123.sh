#!/bin/bash

chmod +x start.sh
chmod +x measure.py
chmod +x explore.py
chmod +x gen_blocks.py 
chmod +x plot_3d.py 


make

echo 'Original phods without transformations'  
python3 measure_all.py ./x1 500
echo 'phods with merge transformation'  
python3 measure_all.py ./x2 500
echo 'phods with reuse transformation' 
python3 measure_all.py ./x3 500 

rm x1 x2 x3 x4 x5 mq
