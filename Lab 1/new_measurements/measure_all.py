import sys
import numpy as np
from subprocess import check_output



def fun(cmd,n):
    s = 0
    arr=np.zeros((n,), dtype=int)

    for i in range(n):
         arr[i]= int(check_output(cmd).decode())
         s += arr[i]

    return arr


if __name__ == '__main__':
    cmd = [
        './' + sys.argv[1]]
    times = int(sys.argv[2])
    time_arr=fun(cmd,times)
    print(time_arr)
